from flask import Flask, render_template, request
import filemanager as fm
import mover as mv


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/control')
def generatepanel():
    cars = fm.generatecontrol()
    return render_template('control.html', cars=cars)


@app.route('/mover', methods=['POST', 'GET'])
def mover():
    if request.method == 'POST':
        data = request.form.to_dict()
        if 'con' in data:
            estado = mv.mover(request.form['id'],request.form['lat'],request.form['lon'])
            fm.writelog(data,estado)
            return "<h1>ok</h1>"
        return "<h2>Se requiere confirmacion</h2>"

    '#habria que controlar que no esten vacios los campos del form, para que no llegues aca por error'
    '#llamaria a la funcion de mover'
    '#mover() ----> de mover.py mejor hacerlo el otro archivo porque va a estar aspero'
    '#que te tire un check el mover() o algun control para loguear lo que paso'


@app.route('/info')
def info():
    return render_template('info.html')


if __name__ == '__main__':
    app.run(debug =True)
