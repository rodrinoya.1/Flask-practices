import filemanager as fm

def mover(id,lat,lon):
    ide = id
    lat = lat
    lon = lon
    print("\nMoviendo a:\t{} , {}".format(lat,lon))
    
    #Algoritmo

    #podria hacer que esta funcion te devielva true or false ó 0 para que despues se escriba en el log
    #entonces si ocurrio algun problema se pondria en el log que se movio pero con error (no se movio)
    
    #luego esta funcion deberia buscar cada archivo de cada robot que se movio y cambiarle la posicion
    #pero como es algo de archivos lo voy a definir en el filemanager.py

    #if todo sale bien:
    fm.writeposrobot(ide, lat, lon)
    return 0
    #endif