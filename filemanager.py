import csv
import os.path
import os

#funcion para generar el panel de control con cada archivo en /files/robotX, lee la ultima linea y saca lat,long
def generatecontrol():
    i = 0
    autos_ll = {}

    DIR = 'files/robots'
    x = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])
    print ("\nCantidad de robots: {}".format(x))
    print ("\nGenerando control...\n")

    for i in range (0, x):
        tmp = "files/robots/robot{}".format(i+1)
        fd = open(tmp)
        autos_tmp = (fd.read()).splitlines()
        print ("opened: files/robot{} ----> {}".format(i+1,autos_tmp[-1]))
        autos_ll[i+1] = autos_tmp[-1]
        fd.close()
        if i == (x-1):
            print ("\nEnviando...\n{}\n".format(autos_ll))
            return autos_ll


# id del auto, latitud , long, y el estado, ok o err(opcional el ult arg)
def writelog(data,estado):
    csvdata = {}
    csvdata = data
    error = estado
    csvdata['err'] = error
    del csvdata['con']
    del csvdata['sta']
    columns = ['id', 'lat', 'lon', 'err']
    if not os.path.exists('files/log/poslog.csv'): 
        os.mknod('files/log/poslog.csv')
        with open('files/log/poslog.csv', "a") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=columns)
            writer.writeheader()
            writer.writerow(csvdata)
    else:
        with open('files/log/poslog.csv', "a",) as csvfile:
            print ("\nLog abierto")
            print ("{}".format(csvdata))
            writer = csv.DictWriter(csvfile, fieldnames=columns)
            writer.writerow(csvdata)
            print ("Log escrito\n")
        
def writeposrobot(ide, lat, lon):
    ide = ide
    lat = lat
    lon = lon
    #sta = estado (habilitado 1 deshabilitado 0) por ahora no se van a deshabilitar por lo que default 1
    sta = 1
    data = "{},{},{},{}\n".format(ide,lat,lon,sta)
    nombretemp = "files/robots/robot{}".format(ide)
    with open(nombretemp, "a") as fdrobot:
        fdrobot.write(data)